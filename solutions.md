# Solutions

## 1 - Sous-ensembles

> Écrivez un prédicat en Prolog qui permet d'énumérer tous les "sous-ensembles"
> d'une liste triée. Plus précisément, on s'attend au comportement suivant:
> ```prolog
> ?- sous_ensembles([2,4,5], L]).
> L = [[], [2], [4], [5], [2, 4], [2, 5], [4, 5], [2, 4, 5]].
> ```

```prolog
puissance([], []).
puissance([_|Xs], P)     :- puissance(Xs,P).
puissance([X|Xs], [X|P]) :- puissance(Xs,P).

sous_ensemble(L, P) :- setof(P, puissance(L, P), P).
```

## 2 - Coupures

### 2.1 - Optimisation

> Considérez le prédicat suivant:
> ```prolog
> signe(N, plus) :- N > 0.
> signe(0, zero).
> signe(N, moins) :- N < 0.
> ```
> Réécrivez-le à l'aide de coupures, de telle sorte qu'il ait exactement le même
> comportement, mais qu'il soit plus efficace (il ne fait aucune vérification
> inutile).

```prolog
signe(0, zero)  :- !.
signe(N, plus)  :- N > 0, !.
signe(N, moins) :- N < 0.
```

### 2.2 - Séparation

> Sans utiliser de coupures, implémentez un prédicat
> ```prolog
> separer(L, P, N)
> ```
> qui retourne vrai si et seulement si les valeurs de `P` et les valeurs de `N`
> sont respectivement les valeurs positives et négatives de la liste `L`. Par
> exemple, on s'attend à obtenir
> ```prolog
> ?- separer([2,-1,4,7,-8,0,-2], P, N).
> P = [2, 4, 7, 0]
> N = [-1, -8, -2]
> ```
> Ensuite, optimisez votre prédicat à l'aide de coupures qui ne modifient pas le
> résultat, mais seulement l'efficacité du calcul.

```prolog
separer([], [], []) :- !.
separer([P|Xs], [P|Ps], N) :-
    (signe(P, plus) ; signe(P, zero)),
    separer(Xs, Ps, N), !.
separer([N|Xs], P, [N|Ns]) :-
    signe(N, moins),
    separer(Xs, P, Ns).
```

## 3 - Énumération de chemins

### 3.1 - Être dans un rectangle

> Implémentez un prédicat `dans_rectangle(X, Y, W, H)` qui indique si le point
> `(X,Y)` se trouve dans le rectangle $`(1,W) \times (1,H)`$. On s'attend donc à
> ce que
> ```prolog
> dans_rectangle(1, 5, 4, 7)
> ```
> retourne vrai. Le prédicat prédifini ``between/3`` vous sera utile.

```prolog
dans_rectangle(X, Y, W, H) :- between(1, W, X), between(1, H, Y).
```

### 3.2 - Rectangle complet

> En utilisant le prédicat défini à la sous-question précédente, proposez un
> prédicat `rectangle(W, H, P)` qui est vérifié si `P` est une liste qui contient
> toutes les positions qui se trouvent dans le rectangle $`(1,W) \times (1,H)`$.
>
> Le prédicat `findall/3` pourrait vous être utile. Il est également intéressant
> de comparer le résultat obtenu en utilisant les prédicats `bagof/3` et
> `setof/3`.

```prolog
rectangle(W, H, P) :- findall((X, Y), dans_rectangle(X, Y, W, H), P).
```

Aussi, puisque j'avais (Simon) une autre interprétation de ce qui était demandé à la
base, on peut vérifier que l'ensemble des points passés sont dans le rectangle,
mais pas juste égale au rectangle:

```prolog
points_dans_rectangle(W, H, P) :-
    findall((X, Y), dans_rectangle(X, Y, W, H), Xs),
    intersection(Xs, P, P).
```

### 3.3 - Chemins

> **Note**: Je ne m'attends pas à ce que vous ayez terminé cette question pendant
> ce laboratoire, mais comme elle nécessite une certaine réflexion, je vous
> encourage à la commencer et à la compléter la semaine prochaine.
>
> Implémentez un prédicat `chemins(X1, Y1, X2, Y2, W, H, C)` qui énumère tous les
> chemins du point `p(X1, Y1)` vers le point `p(X2, Y2)` passant seulement par
> des points du rectangle de largeur `W` et de hauteur `H`, et qui stocke le
> résultat dans `C`.
>
> On représente un chemin par une liste de points qui sont adjacents deux à deux.
> Par exemple, un chemin du point `p(2,4)` au point `p(3,0)` pourrait être donné
> par la liste
> ```prolog
> [p(2,4), p(2,3), p(3,3), p(3,2), p(3,1), p(3,0)]
> ```
>
> Notez que, en principe, le nombre de chemins possible est infini. Par
> conséquent, nous allons ajouter la contrainte qu'il est interdit de passer plus
> d'une fois par le même point.
>
> **Indice 1**: Il peut être utile de définir un prédicat
> ```prolog
> adjacent(X1, Y1, X2, Y2)
> ```
> qui indique si les points `(X1, Y1)` et `(X2, Y2)` sont adjacents.
>
> **Indice 1**: Utilisez un prédicat auxiliaire
> ```prolog
> chemin(X1, Y1, X2, Y2, W, H, C, I)
> ```
> qui calcule un chemin `C` de `(X1, Y1)` vers `(X2, Y2)` dans un rectangle `W`
> par `H` et qui ne peut pas passer par les points présents dans la liste `I`
> (bref, `I` signifie *interdit* dans ce contexte). Vous pourrez ensuite plus
> facilement définir le prédicat `chemins`.


```prolog
adjacent(X1, Y1, X2, Y2) :-
    1 is abs(Y2 - Y1) + abs(X2 - X1).

chemin(X1, Y1, X2, Y2, W, H, C) :-
    chemin(X1, Y1, X2, Y2, W, H, C, [p(X1, Y1)]).

chemin(X, Y, X, Y, W, H, [p(X, Y)], _) :-
    dans_rectangle(X, Y, W, H).

chemin(X1, Y1, X2, Y2, W, H, [p(X1, Y1)|C], I) :-
    dans_rectangle(X1, Y1, W, H),
    dans_rectangle(X2, Y2, W, H),
    dans_rectangle(X3, Y3, W, H),
    adjacent(X1, Y1, X3, Y3),
    P = p(X3, Y3),
    \+ member(P, I),
    chemin(X3, Y3, X2, Y2, W, H, C, [P|I]).

chemins(X1, Y1, X2, Y2, W, H, Chemins) :-
    findall(C, chemin(X1, Y1, X2, Y2, W, H, C), Chemins).
```

Aussi, rendez-vous sur le [dépôt][] d'@angetato afin d'apprécier une solution
alternative.

[dépôt]: https://gitlab.com/angetato/INF2160_H2018_Solutions/blob/master/Labo9/Labo9_Ange.md

<!-- vim: set ts=4 sw=4 tw=80 et :-->

